# Tetris

Simple tetris implementation using [PixiJS](https://www.pixijs.com/).

Use arrows to move blocks around. Use space or up arrow to rotate a block.

## Development

After cloning the repo install all dependencies with

```
npm install
```

To start application for development run

```
npm run start
```

This will open wepack dev server with live reloading enabled.

In order to create application production bundle run
```
npm run build
```

### Other commands

```sh
npm run test # run tests
npm run lint # run code linter
npm run lint:fix # fix all fixable lint issues
```
