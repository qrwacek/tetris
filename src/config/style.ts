export const FONT_TEXT = '"Times New Roman", Times, serif';
export const FONT_HEADER = 'Bungee Shade';

export const GUTTER = 20;

export const BUTTON_SIZE = 50;
