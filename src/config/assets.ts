/**
 * Directory containing game assets
 */
export const ASSETS_ROOT = './assets';

export enum GameAsset {
  BACKGROUND = 'background',
  BLOCKS = 'blocks',
  CONTROLS = 'controls',
  BG_MUSIC = 'bg-music',
  SCORE_SOUND = 'score-sound',
}

export const GameAssets: Record<GameAsset, string> = {
  [GameAsset.BACKGROUND]: 'bg_full.jpg',
  [GameAsset.BLOCKS]: 'blocks.json',
  [GameAsset.CONTROLS]: 'controls.json',
  [GameAsset.BG_MUSIC]: 'kingstux-tetris-trance-2.mp3',
  [GameAsset.SCORE_SOUND]: 'coin.wav',
};
