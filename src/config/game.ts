import { BLACK } from '../const/colors';

/**
 * Maximum game canvas width
 */
export const MAX_GAME_WIDTH = 1280;

/**
 * Maximum game canvas height
 */
export const MAX_GAME_HEIGHT = 1024;

/**
 * Default brick size
 */
export const BRICK_SIZE = 42;

/**
 * Brick size for small screens
 */
export const BRICK_SIZE_SMALL = 21;

/**
 * Game canvas background color
 */
export const GAME_BACKGROUND = BLACK;

/**
 * Number of points gained for clearing a row
 */
export const POINTS_PER_ROW = 10;

/**
 * Points interval used to increase speed
 */
export const SPEED_INCREASE_INTERVAL = 100;
