import { Graphics } from 'pixi.js';

import { BLACK } from '../../const/colors';
import Animations from '../../utils/animations';
import { removeElement } from '../../utils/elements';
import ResizableContainer from './resizable-container';

const BACKDROP_BG_COLOR = BLACK;
const BACKDROP_ALPHA = 0.8;
const DEFAULT_FADE_DURATION = 300;

/**
 * Screen with semitransparent backdrop
 */
export default class Screen extends ResizableContainer {
  protected static FADE_DURATION: number = DEFAULT_FADE_DURATION;

  private backdrop?: Graphics;

  public async show(): Promise<void> {
    this.visible = true;
    await Animations.fadeIn(this, Screen.FADE_DURATION);
  }

  public async hide(): Promise<void> {
    await Animations.fadeOut(this, Screen.FADE_DURATION);
    this.visible = false;
  }

  protected draw(): void {
    if (this.backdrop) {
      removeElement(this.backdrop);
    }

    const graphics = new Graphics();
    graphics.beginFill(BACKDROP_BG_COLOR, BACKDROP_ALPHA);
    graphics.drawRect(0, 0, this.screenWidth, this.screenHeight);

    this.addChildAt(graphics, 0);
    this.backdrop = graphics;
    this.interactive = true;
  }
}
