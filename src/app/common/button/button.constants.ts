export const BUTTON_PLAY = 'play';
export const BUTTON_PAUSE = 'pause';
export const BUTTON_SOUND = 'sound';
export const BUTTON_MUSIC = 'music';
export const BUTTON_INFO = 'info';

export type ButtonType = 'play' | 'pause' | 'sound' | 'music' | 'info';
