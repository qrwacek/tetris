import { Sprite, Texture } from 'pixi.js';
import { GameAsset } from '../../../config/assets';
import { getImageFromSprite } from '../../../utils/assets';
import { ButtonType } from './button.constants';

const STATE_DEFAULT = '';
const STATE_HOVER = 'hover';
const STATE_CLICK = 'click';
const STATE_DISABLED = 'disabled';

type ButtonState = '' | 'hover' | 'click' | 'disabled';

/**
 * Class for creating buttons with states (default, hover, click , disabled).
 */
export default class Button extends Sprite {
  private isDisabled = false;

  private static getImage(type: string): Texture {
    return getImageFromSprite(GameAsset.CONTROLS, type);
  }

  constructor(private type: ButtonType) {
    super(Button.getImage(type));

    this
      .on('pointerover', () => {
        this.setState(STATE_HOVER);
      })
      .on('pointerout', () => {
        this.setState(STATE_DEFAULT);
      })
      .on('pointerupoutside', () => {
        this.setState(STATE_DEFAULT);
      })
      .on('pointerdown', () => {
        this.setState(STATE_CLICK);
      })
      .on('pointerup', () => {
        this.setState(STATE_DEFAULT);
        if (!this.disabled) {
          this.emit('action');
        }
      });

    this.interactive = true;
    this.buttonMode = true;
  }

  private setState(state: ButtonState): void {
    this.texture = this.getStateImage(this.isDisabled ? STATE_DISABLED : state);
  }

  private getStateImage(state: ButtonState): Texture {
    return Button.getImage(state === STATE_DEFAULT ? this.type : `${this.type}_${state}`);
  }

  public get disabled(): boolean {
    return this.isDisabled;
  }

  public set disabled(isDisabled: boolean) {
    this.isDisabled = isDisabled;

    this.texture = this.getStateImage(isDisabled ? STATE_DISABLED : STATE_DEFAULT);
  }
}
