import { Container } from 'pixi.js';

/**
 * Container able to resize on screen size change
 */
export default abstract class ResizableContainer extends Container {
  protected screenWidth: number = window.innerWidth;

  protected screenHeight: number = window.innerHeight;

  constructor() {
    super();

    this.on('added', () => {
      this.screenWidth = this.parent.width;
      this.screenHeight = this.parent.height;

      this.draw();
    });
  }

  /**
   * Redraw component on screen with new size
   *
   * @param width New screen width
   * @param height New screen height
   */
  public resize(width: number, height: number): void {
    this.screenWidth = width;
    this.screenHeight = height;

    this.draw();
  }

  /**
   * Move component to center of the screen
   *
   * @param offsetX Additional horizontal offset
   * @param offsetY Additional vertical offset
   */
  public center(offsetX = 0, offsetY = 0): void {
    this.x = (this.screenWidth - this.width) / 2 + offsetX;
    this.y = (this.screenHeight - this.height) / 2 + offsetY;
  }

  /**
   * Draw component content utilising screen size
   */
  protected abstract draw(): void;
}
