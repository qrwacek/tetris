import { Container, TilingSprite } from 'pixi.js';
import { GameAsset } from '../config/assets';
import { BRICK_SIZE } from '../config/game';
import { getImageFromSprite } from '../utils/assets';
import { centerElement, removeElement } from '../utils/elements';
import { matrixForEach } from '../utils/matrix';
import {
  Block,
  BlockShape,
  BlockColor,
  Brick,
} from './block';

export interface IBlock {
  block: Block;
  row: number;
  column: number;
}

export const COLUMNS = 10;
export const ROWS = 20;

/**
 * @emits block-drop Emitted when current block hits the bottom
 * @emits score Emitted when a complete row was found
 * @emits no-moves Emitted when there's no more moves available
 */
export default class Blocks extends Container {
  private background?: TilingSprite;

  private blocks?: Container;

  private fallenBlocks!: Array<Array<BlockColor | null>>;

  private currentBlock: IBlock | null = null;

  constructor() {
    super();

    this.clearBlocks();
    this.draw();
  }

  public reset(): void {
    this.clearBlocks();
    this.drawBlocks();
  }

  protected clearBlocks(): void {
    if (this.currentBlock) {
      removeElement(this.currentBlock.block);
      this.currentBlock = null;
    }
    this.fallenBlocks = (new Array(ROWS).fill(null)).map(() => new Array(COLUMNS).fill(null));
  }

  public tick(): void {
    if (
      this.currentBlock
      && this.blockCanFall
    ) {
      this.currentBlock.row += 1;
      this.updateCurrentBlockPosition();
    } else {
      this.onBlockDrop();
      this.emit('block-drop');
    }
  }

  protected get blockCanFall(): boolean {
    if (!this.currentBlock) {
      return false;
    }

    return !this.blockCollides({
      ...this.currentBlock,
      row: this.currentBlock.row + 1,
    });
  }

  protected get blockCanRotate(): boolean {
    if (!this.currentBlock) {
      return false;
    }

    const { shape, color, rotationAngle } = this.currentBlock.block;
    const rotatedBlock = new Block(shape, color, rotationAngle + 1);

    return !this.blockCollides({
      ...this.currentBlock,
      block: rotatedBlock,
    });
  }

  /**
   * Checkes whether block in given state overflows container or collides with current blocks
   * @param block Block state
   */
  protected blockCollides(block: IBlock): boolean {
    if (block.row < 0 || block.row + block.block.rows > ROWS) { // overflows horizontally
      return true;
    }
    if (block.column < 0 || block.column + block.block.columns > COLUMNS) { // overflows vertically
      return true;
    }

    // check collision with other blocks
    const positionsToCheck: [number, number][] = [];
    Blocks.forEachBlockPosition(block, (row, column) => {
      positionsToCheck.push([row, column]);
    });
    if (positionsToCheck.some(([row, column]) => this.fallenBlocks[row][column] !== null)) {
      return true;
    }

    return false;
  }

  protected static forEachBlockPosition(
    block: IBlock,
    callback: (row: number, column: number) => void,
  ): void {
    const currentBlockRow = block.row;
    const currentBlockColumn = block.column;

    matrixForEach(block.block.matrix, (item, itemRow, itemColumn) => {
      if (item) {
        callback(currentBlockRow + itemRow, currentBlockColumn + itemColumn);
      }
    });
  }

  protected onBlockDrop(): void {
    if (!this.currentBlock) {
      return;
    }

    // move current block to fallen blocks
    const { color } = this.currentBlock.block;
    Blocks.forEachBlockPosition(this.currentBlock, (row, column) => {
      this.fallenBlocks[row][column] = color;
    });

    // check for complete rows
    const start = this.currentBlock.row;
    const end = start + this.currentBlock.block.rows;
    let completeRows = 0;
    for (let i = start; i < end; i += 1) {
      if (this.fallenBlocks[i].every((item) => item !== null)) {
        completeRows += 1;
        for (let j = i; j > 0; j -= 1) {
          this.fallenBlocks[j] = [...this.fallenBlocks[j - 1]];
        }
      }
    }

    if (completeRows > 0) {
      this.emit('score', completeRows);
    }

    this.drawBlocks();
  }

  public addBlock(shape: BlockShape, color: BlockColor): void {
    if (this.currentBlock) {
      removeElement(this.currentBlock.block);
    }

    const block = new Block(shape, color);
    // center new block
    const column = Math.floor((COLUMNS - block.columns) / 2);
    const newBlock: IBlock = {
      block,
      row: 0,
      column,
    };

    if (this.blockCollides(newBlock)) {
      this.emit('no-moves');
      return;
    }

    this.currentBlock = newBlock;
    this.updateCurrentBlockPosition();
    this.blocks?.addChild(this.currentBlock.block);
  }

  public rotateBlock(): void {
    if (this.currentBlock && this.blockCanRotate) {
      this.currentBlock.block.rotate();
    }
  }

  public moveBlockDown(): void {
    if (this.currentBlock && this.blockCanFall) {
      this.currentBlock.row += 1;
      this.updateCurrentBlockPosition();
    }
  }

  public moveBlockLeft(): void {
    this.moveBlock(-1);
  }

  public moveBlockRight(): void {
    this.moveBlock(1);
  }

  protected moveBlock(by: number): void {
    if (!this.currentBlock) {
      return;
    }

    const newColumn = this.currentBlock.column + by;

    const willCollide = this.blockCollides({
      ...this.currentBlock,
      column: newColumn,
    });

    if (!willCollide) {
      this.currentBlock.column = newColumn;
      this.updateCurrentBlockPosition();
    }
  }

  public draw(): void {
    this.drawBackground();
    this.drawBlocks();
    centerElement(this);
  }

  protected drawBackground(): void {
    if (this.background) {
      removeElement(this.background);
    }

    const backgroundImage = getImageFromSprite(GameAsset.BLOCKS, 'bg');
    const width = BRICK_SIZE * COLUMNS;
    const height = BRICK_SIZE * ROWS;
    this.background = new TilingSprite(backgroundImage, width, height);
    this.addChild(this.background);
  }

  protected drawBlocks(): void {
    // prepare container
    if (this.blocks) {
      removeElement(this.blocks);
    }
    this.blocks = new Container();

    // draw fallen blocks
    matrixForEach(this.fallenBlocks, (color, row, column) => {
      if (color) {
        const x = BRICK_SIZE * column;
        const y = BRICK_SIZE * row;
        const brick = new Brick(color);
        brick.position.set(x, y);
        this.blocks?.addChild(brick);
      }
    });

    // draw current block
    if (this.currentBlock) {
      this.blocks.addChild(this.currentBlock.block);
      this.updateCurrentBlockPosition();
    }

    this.addChild(this.blocks);
  }

  protected updateCurrentBlockPosition(): void {
    if (!this.currentBlock) {
      return;
    }
    const { block, column, row } = this.currentBlock;
    const x = BRICK_SIZE * column;
    const y = BRICK_SIZE * row;
    block.position.set(x, y);
  }
}
