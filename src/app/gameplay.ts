import { Sprite, Ticker } from 'pixi.js';
import { GameAsset } from '../config/assets';
import { BRICK_SIZE, POINTS_PER_ROW, SPEED_INCREASE_INTERVAL } from '../config/game';
import { GUTTER } from '../config/style';
import { getImage, getSound } from '../utils/assets';
import { centerElement, removeElement } from '../utils/elements';
import ResizableContainer from './common/resizable-container';
import {
  BlockColor,
  BlockShape,
  getRandomColor,
  getRandomShape,
} from './block';
import Blocks, { ROWS, COLUMNS } from './blocks';
import Score from './score';
import NextBlock from './next-block';
import Controls from './controls';

const BLOCKS_WIDTH = BRICK_SIZE * COLUMNS;
const BLOCKS_HEIGHT = BRICK_SIZE * ROWS;

/**
 * @emits start Emitted when gameplay is started
 * @emits pause Emitted when gameplay is paused
 * @emits game-over Emitted when game has ended
 */
export class Gameplay extends ResizableContainer {
  private bg!: Sprite;

  private blocks!: Blocks;

  private score!: Score;

  private nextBlock!: NextBlock;

  private nextBlockShape!: BlockShape;

  private nextBlockColor!: BlockColor;

  private controls!: Controls;

  private ticker: Ticker;

  private elapsedSeconds = 0;

  private totalScore = 0;

  private speed = 1;

  private bgMusic = getSound(GameAsset.BG_MUSIC);

  private scoreSound = getSound(GameAsset.SCORE_SOUND);

  constructor() {
    super();

    this.ticker = new Ticker();
    this.ticker.autoStart = false;

    this.bgMusic.loop = true;
    this.bgMusic.volume = 0.5;
  }

  protected get isPaused(): boolean {
    return !this.ticker.started;
  }

  protected draw(): void {
    if (this.score) {
      removeElement(this.score);
    }
    if (this.nextBlock) {
      removeElement(this.nextBlock);
    }
    if (this.controls) {
      removeElement(this.controls);
    }

    this.adjustScale();

    this.drawBlocks();

    centerElement(this);

    this.drawScore();
    this.drawNextBlock();
    this.drawControls();
  }

  /**
   * Scale to screen
   */
  protected adjustScale(): void {
    const maxHeight = ROWS * BRICK_SIZE;
    const viewportHeight = this.screenHeight - 2 * GUTTER;

    if (viewportHeight > maxHeight) {
      this.scale.set(1);
    } else {
      this.scale.set(viewportHeight / maxHeight);
    }
  }

  protected drawBackground(): void {
    if (this.bg) {
      removeElement(this.bg);
    }
    this.bg = new Sprite(getImage(GameAsset.BACKGROUND));
    this.addChild(this.bg);
  }

  protected drawBlocks(): void {
    if (this.blocks) {
      this.blocks.draw();
      return;
    }

    this.initBlocks();
  }

  protected initBlocks(): void {
    this.blocks = new Blocks();

    this.blocks.on('block-drop', () => {
      this.addNewBlock();
    });

    this.blocks.on('score', (rows: number) => {
      this.onScore(rows);
    });

    this.blocks.on('no-moves', () => {
      this.onGameOver();
    });

    this.ticker.add((delta) => {
      this.elapsedSeconds += (1 / 60) * delta;
      if (this.elapsedSeconds >= 1 / this.speed) {
        this.blocks.tick();
        this.elapsedSeconds = 0;
      }
    });

    window.addEventListener('keydown', (event: KeyboardEvent) => {
      if (event.key === 'ArrowLeft') {
        this.blocks.moveBlockLeft();
      }
      if (event.key === 'ArrowRight') {
        this.blocks.moveBlockRight();
      }
      if (event.key === 'ArrowDown') {
        this.blocks.moveBlockDown();
      }
      if (event.key === 'ArrowUp' || event.key === ' ') {
        this.blocks.rotateBlock();
      }
    });

    this.addChild(this.blocks);
  }

  protected chooseNextBlock(): void {
    this.nextBlockShape = getRandomShape();
    this.nextBlockColor = getRandomColor();
    this.nextBlock.setNextBlock(this.nextBlockShape, this.nextBlockColor);
  }

  protected addNewBlock(): void {
    this.blocks.addBlock(this.nextBlockShape, this.nextBlockColor);
    this.chooseNextBlock();
  }

  protected drawScore(): void {
    this.score = new Score(this.totalScore);
    this.score.x = BLOCKS_WIDTH + GUTTER;
    this.addChild(this.score);
  }

  protected onScore(rows: number): void {
    this.totalScore += rows * POINTS_PER_ROW;
    this.speed = Math.floor(this.totalScore / SPEED_INCREASE_INTERVAL) + 1;
    this.score.setScore(this.totalScore);
    this.scoreSound.play();
    this.scoreSound.play();
    this.scoreSound.play();
  }

  protected drawNextBlock(): void {
    this.nextBlock = new NextBlock();
    this.nextBlock.x = BLOCKS_WIDTH + GUTTER;
    this.nextBlock.y = BLOCKS_HEIGHT / 2;
    this.addChild(this.nextBlock);

    this.chooseNextBlock();
  }

  protected drawControls(): void {
    this.controls = new Controls();
    this.controls.on('toggle-music', () => {
      this.bgMusic.muted = !this.bgMusic.muted;
    });
    this.controls.on('toggle-sound', () => {
      this.scoreSound.muted = !this.scoreSound.muted;
    });
    this.controls.on('toggle-pause', () => {
      if (this.isPaused) {
        this.resume();
      } else {
        this.pause();
      }
    });
    this.controls.x = -this.controls.width - GUTTER;
    this.addChild(this.controls);
  }

  protected onGameOver(): void {
    this.pause();
    this.emit('game-over');
  }

  protected reset(): void {
    this.speed = 1;
    this.totalScore = 0;
    this.score.setScore(this.totalScore);

    this.blocks.reset();
    this.addNewBlock();
  }

  public start(): void {
    if (!this.isPaused) {
      return;
    }

    this.reset();
    this.resume();
  }

  public pause(): void {
    if (this.isPaused) {
      return;
    }

    this.bgMusic.stop();
    this.ticker.stop();
  }

  public resume(): void {
    if (!this.isPaused) {
      return;
    }

    this.ticker.start();
    this.bgMusic.play();
  }
}

export default Gameplay;
