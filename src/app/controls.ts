import { Container } from 'pixi.js';
import { GUTTER } from '../config/style';
import { Button, ButtonType } from './common/button';

const BUTTON_SIZE = 60;

/**
 * @emits toggle-sound Emitted when sound button is clicked
 * @emits toggle-music Emitted when music button is clicked
 * @emits toggle-pause Emitted when pause button is clicked
 */
export default class Controls extends Container {
  constructor() {
    super();

    this.drawButtons();
  }

  protected drawButtons(): void {
    this.createButton('sound', 'toggle-sound');
    this.createButton('music', 'toggle-music');
    this.createButton('pause', 'toggle-pause', true);
  }

  protected createButton(buttonType: ButtonType, emitedEvent: string, noToggle = false): void {
    const button = new Button(buttonType);
    button.on('action', () => {
      this.emit(emitedEvent);
      if (!noToggle) {
        button.alpha = button.alpha === 1 ? 0.5 : 1;
      }
    });
    button.width = BUTTON_SIZE;
    button.height = BUTTON_SIZE;
    button.y = (BUTTON_SIZE + GUTTER / 2) * this.children.length;
    this.addChild(button);
  }
}
