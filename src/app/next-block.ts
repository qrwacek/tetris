import { Text, TextStyle } from 'pixi.js';
import { FONT_HEADER, GUTTER } from '../config/style';
import { WHITE } from '../const/colors';
import { removeElement } from '../utils/elements';
import { Block, BlockColor, BlockShape } from './block';
import ResizableContainer from './common/resizable-container';

export default class NextBlock extends ResizableContainer {
  protected shape?: BlockShape;

  protected color?: BlockColor;

  protected text!: Text;

  protected nextBlock?: Block;

  public setNextBlock(shape: BlockShape, color: BlockColor): void {
    this.shape = shape;
    this.color = color;

    this.drawBlock();
  }

  protected draw(): void {
    this.drawText();
    this.drawBlock();
  }

  protected drawText(): void {
    if (this.text) {
      removeElement(this.text);
    }
    const textStyle = new TextStyle({
      fontFamily: FONT_HEADER,
      fill: WHITE,
    });
    this.text = new Text('NEXT', textStyle);
    this.addChild(this.text);
  }

  protected drawBlock(): void {
    if (this.nextBlock) {
      removeElement(this.nextBlock);
    }
    if (this.shape && this.color) {
      this.nextBlock = new Block(this.shape, this.color);
      this.nextBlock.y = this.text.height + GUTTER;
      this.addChild(this.nextBlock);
    }
  }
}
