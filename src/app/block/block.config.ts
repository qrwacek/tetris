import { BlockMatrix, BlockShape } from './block.types';

const BlockConfig: Record<BlockShape, BlockMatrix> = {
  [BlockShape.I]: [
    [1, 1, 1, 1],
  ],
  [BlockShape.L]: [
    [1, 0],
    [1, 0],
    [1, 1],
  ],
  [BlockShape.J]: [
    [0, 1],
    [0, 1],
    [1, 1],
  ],
  [BlockShape.S]: [
    [0, 1, 1],
    [1, 1, 0],
  ],
  [BlockShape.T]: [
    [1, 1, 1],
    [0, 1, 0],
  ],
  [BlockShape.Z]: [
    [1, 1, 0],
    [0, 1, 1],
  ],
  [BlockShape.O]: [
    [1, 1],
    [1, 1],
  ],
};

export default BlockConfig;
