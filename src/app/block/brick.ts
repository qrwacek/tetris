import { Container, Sprite } from 'pixi.js';
import { GameAsset } from '../../config/assets';
import { getImageFromSprite } from '../../utils/assets';
import { BlockColor } from './block.types';

export default class Brick extends Container {
  constructor(color: BlockColor) {
    super();

    const brick = new Sprite(getImageFromSprite(GameAsset.BLOCKS, color));
    this.addChild(brick);
  }
}
