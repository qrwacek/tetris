import { Container } from 'pixi.js';
import { matrixForEach, rotateMatrix } from '../../utils/matrix';
import BlockConfig from './block.config';
import { BlockColor, BlockMatrix, BlockShape } from './block.types';
import Brick from './brick';

export default class Block extends Container {
  private blockMatrix: BlockMatrix;

  constructor(
    private blockShape: BlockShape,
    private blockColor: BlockColor,
    private blockRotation = 0,
  ) {
    super();

    this.blockMatrix = BlockConfig[blockShape];
    for (let i = 0; i < blockRotation; i += 1) {
      this.blockMatrix = rotateMatrix(this.blockMatrix);
    }

    this.draw();
  }

  public get shape(): BlockShape {
    return this.blockShape;
  }

  public get color(): BlockColor {
    return this.blockColor;
  }

  public get rotationAngle(): number {
    return this.blockRotation;
  }

  public get matrix(): BlockMatrix {
    return this.blockMatrix;
  }

  public get rows(): number {
    return this.blockMatrix.length;
  }

  public get columns(): number {
    return this.blockMatrix[0].length;
  }

  public rotate(): void {
    this.blockMatrix = rotateMatrix(this.blockMatrix);
    this.blockRotation = (this.blockRotation + 1) % 4;
    this.draw();
  }

  protected draw(): void {
    this.removeChildren();

    matrixForEach(this.blockMatrix, (item, row, column) => {
      if (item) {
        const element = new Brick(this.blockColor);
        const x = element.width * column;
        const y = element.height * row;
        element.position.set(x, y);
        this.addChild(element);
      }
    });
  }
}
