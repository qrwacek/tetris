export enum BlockShape {
  S = 'S',
  Z = 'Z',
  T = 'T',
  L = 'L',
  I = 'I',
  J = 'J',
  O = 'O',
}

export type BlockMatrix = number[][];

export enum BlockColor {
  BLUE = 'blue',
  GREEN = 'green',
  LIGHT_BLUE = 'light_blue',
  ORANGE = 'orange',
  PINK = 'pink',
  RED = 'red',
  VIOLET = 'violet',
  YELLOW = 'yellow',
}
