import { Text, TextStyle } from 'pixi.js';
import { FONT_HEADER, GUTTER } from '../config/style';
import { WHITE } from '../const/colors';
import ResizableContainer from './common/resizable-container';

export default class Score extends ResizableContainer {
  private text?: Text;

  private scoreText?: Text;

  constructor(private score = 0) {
    super();
  }

  public setScore(score: number): void {
    this.score = score;
    if (this.scoreText) {
      this.scoreText.text = `${score}`;
    }
  }

  protected draw(): void {
    this.removeChildren();
    this.drawText();
    this.drawScore();
  }

  protected drawText(): void {
    const textStyle = new TextStyle({
      fontFamily: FONT_HEADER,
      fill: WHITE,
    });
    this.text = new Text('SCORE', textStyle);
    this.addChild(this.text);
  }

  protected drawScore(): void {
    const textStyle = new TextStyle({
      fill: WHITE,
      fontFamily: FONT_HEADER,
      align: 'center',
      fontSize: 40,
    });
    this.scoreText = new Text(`${this.score}`, textStyle);
    this.scoreText.y = this.text?.height || 0 + GUTTER;
    this.addChild(this.scoreText);
  }
}
