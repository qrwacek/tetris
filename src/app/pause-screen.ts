import animations from '../utils/animations';
import { centerElement } from '../utils/elements';
import { Button } from './common/button';
import Screen from './common/screen';

export default class PauseScreen extends Screen {
  private button!: Button;

  constructor() {
    super();
    this.on('added', this.draw.bind(this));
  }

  public draw(): void {
    super.draw();
    this.drawButton();
  }

  public async show(): Promise<void> {
    await Promise.all([
      super.show(),
      animations.grow(this.button, Screen.FADE_DURATION),
    ]);
  }

  public async hide(): Promise<void> {
    await Promise.all([
      super.hide(),
      animations.shrink(this.button, Screen.FADE_DURATION),
    ]);
  }

  private drawButton(): void {
    if (!this.button) {
      const button = new Button('play');
      button.on('action', () => {
        this.emit('button-click');
      });

      this.addChild(button);
      this.button = button;
    }

    centerElement(this.button);
  }
}
