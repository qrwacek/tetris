import {
  Application,
  Container,
  DisplayObject,
  Graphics,
  ISize,
} from 'pixi.js';
import 'pixi-sound';
import { GameAsset } from './config/assets';
import { GAME_BACKGROUND, MAX_GAME_HEIGHT, MAX_GAME_WIDTH } from './config/game';
import { FONT_HEADER } from './config/style';
import { loadAssets, loadFont } from './utils/assets';
import { Gameplay } from './app/gameplay';
import PauseScreen from './app/pause-screen';
import { removeElement } from './utils/elements';
import { BLACK } from './const/colors';

export default class Game {
  /**
   * Application instance
   */
  private app: Application;

  /**
   * Root game container
   */
  private content!: Container;

  /**
   * Main background
   */
  private background!: Graphics;

  /**
   * Gameplay container
   */
  private gameplay!: Gameplay;

  /**
   * Pause screen container
   */
  private pauseScreen!: PauseScreen;

  /**
   * Create application instance, attach it to DOM and add listeners
   * @param domNode DOM node to attach to
   */
  constructor(private domNode: Element = document.documentElement) {
    const { width, height } = this.calculateSize();

    // instantiate app
    this.app = new Application({
      width,
      height,
      backgroundColor: GAME_BACKGROUND,
      antialias: true,
    });
    this.app.renderer.view.style.position = 'absolute';
    this.app.renderer.view.style.display = 'block';

    // debounced resize on window size change
    let resizeTimer: number;
    window.addEventListener('resize', () => {
      clearTimeout(resizeTimer);
      resizeTimer = window.setTimeout(() => {
        this.resize();
      }, 250);
    });

    // create view in DOM
    this.domNode.appendChild(this.app.view);
  }

  /**
   * Start loading game
   */
  public async init(): Promise<void> {
    await loadAssets([
      GameAsset.BLOCKS,
    ]);
    await loadFont(FONT_HEADER);

    this.load();
  }

  /**
   * Resize game screen and all of its' components.
   * Should be called after viewport size is changed.
   */
  public resize(): void {
    const { width, height } = this.calculateSize();

    if (width === this.app.screen.width && height === this.app.screen.height) {
      return;
    }

    this.app.renderer.resize(width, height);

    this.addBackground();

    if (this.gameplay) {
      this.gameplay.resize(width, height);
    }

    if (this.pauseScreen) {
      this.pauseScreen.resize(width, height);
    }
  }

  /**
   * Calculate size of game screen.
   * If viewport is smaller then configured maximum size game goes full-screen.
   */
  protected calculateSize(): ISize {
    const viewportWidth = this.domNode.clientWidth;
    const viewportHeight = this.domNode.clientHeight;

    if (viewportWidth >= MAX_GAME_WIDTH && viewportHeight >= MAX_GAME_HEIGHT) {
      return {
        width: MAX_GAME_WIDTH,
        height: MAX_GAME_HEIGHT,
      };
    }

    return {
      width: viewportWidth,
      height: viewportHeight,
    };
  }

  /**
   * Load game assets and start game setup
   */
  protected async load(): Promise<void> {
    await loadAssets([
      GameAsset.BACKGROUND,
      GameAsset.CONTROLS,
      GameAsset.BG_MUSIC,
      GameAsset.SCORE_SOUND,
    ]);

    this.setup();
  }

  /**
   * Initialise game components
   */
  protected setup(): void {
    this.content = this.addMainElement(new Container());

    this.addBackground();
    this.setupGameplay();
    this.setupPauseScreen();
  }

  protected addBackground(): void {
    if (this.background) {
      removeElement(this.background);
    }

    this.background = new Graphics()
      .beginFill(BLACK)
      .drawRect(0, 0, this.app.screen.width, this.app.screen.height)
      .endFill();
    this.addElement(this.background, 0);
  }

  protected setupGameplay(): void {
    this.gameplay = this.addElement(new Gameplay());
    this.gameplay.on('game-over', () => {
      this.pauseScreen.show();
    });
  }

  protected setupPauseScreen(): void {
    if (this.pauseScreen) {
      this.pauseScreen.show();
    } else {
      this.pauseScreen = new PauseScreen();
      this.pauseScreen.on('button-click', () => this.onGameStart());
      this.addMainElement(this.pauseScreen);
    }
  }

  protected async onGameStart(): Promise<void> {
    await this.pauseScreen.hide();
    this.gameplay.start();
  }

  /**
   * Add element to root scope
   * @param element Element to add
   */
  protected addMainElement<T extends DisplayObject>(element: T): T {
    this.app.stage.addChild(element);
    return element;
  }

  /**
   * Add element to game container
   * @param element Element to add
   * @param index Element position
   */
  protected addElement<T extends DisplayObject>(element: T, index?: number): T {
    if (typeof index !== 'undefined') {
      this.content.addChildAt(element, index);
    } else {
      this.content.addChild(element);
    }
    return element;
  }
}
