import { centerElement, centerSprite } from './center-element';
import removeElement from './remove-element';

export {
  centerElement,
  centerSprite,
  removeElement,
};
