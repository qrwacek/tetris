import { Container, Sprite } from 'pixi.js';

/**
 * Center element within its' container
 *
 * @param element Element to center
 * @param offsetX Additional horizontal offset
 * @param offsetY Additional vertical offset
 */
export const centerElement = (element: Container, offsetX = 0, offsetY = 0): void => {
  if (!element.parent) { return; }

  const { width: parentWidth, height: parentHeight } = element.parent;
  const { width, height } = element;

  const x = (parentWidth - width) / 2 + offsetX;
  const y = (parentHeight - height) / 2 + offsetY;

  element.position.set(x, y);
};

/**
 * Center sprite within its' container.
 * Supports sprite anchor.
 *
 * @param element
 */
export const centerSprite = (element: Sprite): void => {
  const offsetX = element.anchor.x * element.width;
  const offsetY = element.anchor.y * element.height;

  centerElement(element, offsetX, offsetY);
};
