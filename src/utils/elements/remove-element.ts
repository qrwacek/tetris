import { DisplayObject } from 'pixi.js';

const removeElement = (element: DisplayObject): void => {
  if (element.parent) {
    element.parent.removeChild(element);
  }
  element.destroy();
};

export default removeElement;
