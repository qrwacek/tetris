import WebFont from 'webfontloader';

export default function loadFont(fontFamily: string): Promise<void> {
  return new Promise((resolve, reject) => {
    WebFont.load({
      google: {
        families: [fontFamily],
      },
      active: () => {
        resolve();
      },
      inactive: () => {
        reject();
      },
    });
  });
}
