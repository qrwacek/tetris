import { Loader, LoaderResource, Texture } from 'pixi.js';
import { GameAsset } from '../../config/assets';

export function getAsset(asset: GameAsset): LoaderResource {
  const resource = Loader.shared.resources[asset];
  if (!resource) {
    throw new Error(`Resource '${asset}' not found`);
  }
  return resource;
}

export function getImage(asset: GameAsset): Texture {
  return getAsset(asset).texture;
}

/**
 * Get image from sprite
 *
 * @param asset Sprite asset
 * @param key Image key
 */
export function getImageFromSprite(asset: GameAsset, key: string): Texture {
  return getAsset(asset).textures![`${key}.png`];
}
