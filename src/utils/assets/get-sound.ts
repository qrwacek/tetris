import { Loader } from 'pixi.js';
import * as PIXISound from 'pixi-sound';

export default function getSound(key: string): PIXISound.default.Sound {
  return Loader.shared.resources[key].sound;
}
