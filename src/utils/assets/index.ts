export { getAsset, getImage, getImageFromSprite } from './get-asset';
export { default as getSound } from './get-sound';
export { default as loadAssets } from './load-assets';
export { default as loadFont } from './load-font';
