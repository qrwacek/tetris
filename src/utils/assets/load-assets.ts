import { Loader } from 'pixi.js';

import { ASSETS_ROOT, GameAsset, GameAssets } from '../../config/assets';

export default function loadAssets(
  assets: GameAsset[],
  onProgress?: (loader: Loader) => void,
): Promise<void> {
  const loader = Loader.shared;
  assets.forEach((asset) => {
    loader.add(asset, `${ASSETS_ROOT}/${GameAssets[asset]}`);
  });
  if (onProgress) {
    loader.onProgress.add(onProgress);
  }
  return new Promise((resolve) => {
    loader.load(() => {
      resolve();
    });
  });
}
