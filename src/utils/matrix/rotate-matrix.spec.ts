import rotateMatrix from './rotate-matrix';

describe('rotate-matrix', () => {
  it('correctly rotates a matrix', () => {
    expect(rotateMatrix([
      [1, 2, 3],
      [4, 5, 6],
    ])).toEqual([
      [3, 6],
      [2, 5],
      [1, 4],
    ]);
  });

  describe('using block matrix', () => {
    it('rotates T block', () => {
      expect(rotateMatrix([
        [1, 1, 1],
        [0, 1, 0],
      ])).toEqual([
        [1, 0],
        [1, 1],
        [1, 0],
      ]);
    });

    it('rotates S block', () => {
      expect(rotateMatrix([
        [0, 1, 1],
        [1, 1, 0],
      ])).toEqual([
        [1, 0],
        [1, 1],
        [0, 1],
      ]);
    });

    it('rotates Z block', () => {
      expect(rotateMatrix([
        [1, 1, 0],
        [0, 1, 1],
      ])).toEqual([
        [0, 1],
        [1, 1],
        [1, 0],
      ]);
    });

    it('rotates L block', () => {
      expect(rotateMatrix([
        [1, 0],
        [1, 0],
        [1, 1],
      ])).toEqual([
        [0, 0, 1],
        [1, 1, 1],
      ]);
    });

    it('rotates mirrored L block', () => {
      expect(rotateMatrix([
        [0, 1],
        [0, 1],
        [1, 1],
      ])).toEqual([
        [1, 1, 1],
        [0, 0, 1],
      ]);
    });

    it('rotates I block', () => {
      expect(rotateMatrix([
        [1],
        [1],
        [1],
        [1],
      ])).toEqual([
        [1, 1, 1, 1],
      ]);
    });

    it('rotates O block', () => {
      expect(rotateMatrix([
        [1, 1],
        [1, 1],
      ])).toEqual([
        [1, 1],
        [1, 1],
      ]);
    });
  });
});
