import matrixForEach from './matrix-for-each';

describe('matrix-for-each', () => {
  it('should execute calback for each matrix element providing row and column', () => {
    const matrix = [
      [1, 2, 3],
      [4, 5, 6],
    ];
    const callback = jest.fn();

    matrixForEach(matrix, callback);

    expect(callback).toHaveBeenCalledTimes(6);
    expect(callback).toHaveBeenNthCalledWith(1, 1, 0, 0);
    expect(callback).toHaveBeenNthCalledWith(2, 2, 0, 1);
    expect(callback).toHaveBeenNthCalledWith(3, 3, 0, 2);
    expect(callback).toHaveBeenNthCalledWith(4, 4, 1, 0);
    expect(callback).toHaveBeenNthCalledWith(5, 5, 1, 1);
    expect(callback).toHaveBeenNthCalledWith(6, 6, 1, 2);
  });
});
