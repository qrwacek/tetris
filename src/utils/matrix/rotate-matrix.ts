function transposeMatrix<T>(matrix: T[][]): T[][] {
  return matrix[0].map((_, colIndex) => (
    matrix.map((row) => row[colIndex])
  ));
}

export default function rotateMatrix<T>(matrix: T[][]): T[][] {
  const reversed = matrix.map((row) => [...row].reverse());
  return transposeMatrix(reversed);
}
