type Callback<T> = (item: T, row: number, column: number) => void;

export default function matrixForEach<T>(matrix: T[][], callback: Callback<T>): void {
  matrix.forEach((rowItems, row) => {
    rowItems.forEach((item, column) => {
      callback(item, row, column);
    });
  });
}
