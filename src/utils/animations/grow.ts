import { Container } from 'pixi.js';
import animate from './animate';

const grow = (object: Container, duration?: number): Promise<void> => (
  animate(object.scale, { x: 1, y: 1 }, duration)
);

export default grow;
