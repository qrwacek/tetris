import { Easing, Tween, update } from '@tweenjs/tween.js';
import { DisplayObject, ObservablePoint, Ticker } from 'pixi.js';

Ticker.shared.add((): void => {
  update();
});

const DEFAULT_DURATION = 300;

/**
 * Animate object properties
 *
 * @param object Object to animate
 * @param properties Properties to be animated
 * @param duration Animation duration
 * @returns Promise resolved when animation is complete
 */
const animate = (
  object: DisplayObject | ObservablePoint,
  properties: Record<string, number>,
  duration: number = DEFAULT_DURATION,
): Promise<void> => {
  const tween = new Tween(object);
  tween.to(properties, duration).easing(Easing.Quadratic.Out);

  return new Promise((resolve) => {
    tween.onComplete(() => {
      resolve();
    });
    tween.start();
  });
};

export default animate;
