import { Container } from 'pixi.js';
import animate from './animate';

const shrink = (object: Container, duration?: number): Promise<void> => (
  animate(object.scale, { x: 0, y: 0 }, duration)
);

export default shrink;
