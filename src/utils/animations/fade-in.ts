import { Container } from 'pixi.js';
import animate from './animate';

const fadeIn = (object: Container, duration?: number): Promise<void> => (
  animate(object, { alpha: 1 }, duration)
);

export default fadeIn;
