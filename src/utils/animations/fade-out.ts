import { Container } from 'pixi.js';
import animate from './animate';

const fadeOut = (object: Container, duration?: number): Promise<void> => (
  animate(object, { alpha: 0 }, duration)
);

export default fadeOut;
