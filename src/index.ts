import Game from './app';

const rootElement = document.getElementById('root');
if (rootElement) {
  const game = new Game(rootElement);
  game.init();
} else {
  throw new Error('Root element not found');
}
